/* global jQuery */
'use strict';


/**
 * Progress jQuery
 */
(function ($) {
    $.fn.progress = function (options) {

        var options = $.extend({
            /**
             * start loading value
             * @type {number}
             */
            start: 0,
            /**
             * end loading value
             * @type {number}
             */
            end: 100,
            /**
             * jQuery selector for bar
             * @type {?(string|jQuery)}
             */
            bar: null,
            /**
             * template use to show progress number replaced by `%s`
             * @type {string}
             */
            template: '%s%',
            delay: 200,
            decimals: 0
        }, options);

        return this.each(function (el, i) {
            var that = $(this);
            var progress = options.start;
            var remaining = options.end - progress;
            var intervalId = null;
            var bar = options.bar;

            /** check type of bar element */
            if (typeof bar === 'string' ) {
                /** it's a jquery selector */
                bar = $(bar).get(0);
                bar = $(bar);
            } else if (typeof bar === 'object') {
                /** already a jquery node */
            } else {
                /** something else that we don't care */
                bar = null;
            }

            /**
             * start loading
             * 
             */
            var start = function () {
                render();
                intervalId = setInterval(function () {
                    if (isNaN(progress)) {
                        clearInterval(intervalId);
                        progress = 0;
                    } else {
                        remaining = options.end - progress;
                        progress = progress + (0.15 * Math.pow(1 - Math.sqrt(remaining), 2));
                        render();
                    }
                }, options.delay);
            };

            /**
             * update frames
             * 
             */
            var render = function () {
                var size = Math.round(progress * Math.pow(10, options.decimals)) / Math.pow(10, options.decimals);
                that.text(options.template.replace('%s', size));

                if (null != options.bar) {
                    bar.css('width', progress + '%');
                }
            };

            /**
             * restart loading counter
             * 
             */
            var restart = function () {
                progress = options.start;
                render();
            }

            /**
             * complete progress
             * 
             */
            var complete = function () {
                progress = options.end;
                clearInterval(intervalId);
                render()
            }


            /**
             * bind start event
             */
            $(this).on('start.progress', function () {
                start();
            });

            /**
             * bind restart event
             */
            $(this).on('restart.progress', function () {
                restart();
            });

            /**
             * bind complete event
             */
            $(this).on('complete.progress', function () {
                complete();
            })

        });
    }
})(jQuery);
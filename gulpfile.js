var gulp = require('gulp');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var browser = require('browser-sync');

gulp.task('dev', ['serve', 'watch']);

gulp.task('prod', function () {
    gulp.src('src/jquery.progress.js')
        .pipe(gulp.dest('dist/'))
        .pipe(uglify())
        .pipe(rename({
            extname: '.min.js'
        }))
        .pipe(gulp.dest('dist/'));

});

gulp.task('serve', function () {
    browser.init({
        server: './',
        startPath: './test/index.html'
    })
});

gulp.task('watch', function () {
    gulp.watch(['**/*', '!**/*.css'], browser.reload);
    gulp.watch(['**/*.css'], ['style']);
});

gulp.task('style', function () {
    gulp.src('**/*.css')
        .pipe(browser.stream());
});
